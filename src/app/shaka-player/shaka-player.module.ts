import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ShakaPlayerRoutingModule } from './shaka-player-routing.module';
import { BasicComponent } from './basic/basic.component';
import { HlsComponent } from './hls/hls.component';


@NgModule({
  declarations: [
    BasicComponent,
    HlsComponent
  ],
  imports: [
    CommonModule,
    ShakaPlayerRoutingModule
  ],
  exports: [
    BasicComponent,
    HlsComponent
  ]
})
export class ShakaPlayerModule { }
