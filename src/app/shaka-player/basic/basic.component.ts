import { Component, AfterViewInit, ElementRef, ViewChild, Input } from '@angular/core';

declare let shaka: any;
@Component({
  selector: 'app-basic',
  templateUrl: './basic.component.html',
  styleUrls: ['./basic.component.css']
})
export class BasicComponent implements AfterViewInit {
  @ViewChild('videoPlayer') videoElementRef: ElementRef | undefined;
  @ViewChild('videoContainer') videoContainerRef: ElementRef | undefined;

  @Input('parentUrl') public manifesturi: string;
  @Input('parentBanner') public banner: string;
  @Input('parentAutoplay') public autoplay: boolean;

  videoElement: HTMLVideoElement;
  videoContainerElement: HTMLDivElement;
  player: any;

  videoStatus: string;
  errorInPlaying: boolean = false;
  errorMsg: string;

  fullscreenflag: boolean = false;

  constructor() {}


  ngAfterViewInit(): void {
    shaka.polyfill.installAll();
    if (shaka.Player.isBrowserSupported()) {
      this.videoElement = this.videoElementRef?.nativeElement;
      this.videoContainerElement = this.videoContainerRef?.nativeElement;
      this.initPlayer();
    } else {
      console.error('Browser not supported!');
    }
  }

  private initPlayer() {
    this.player = new shaka.Player(this.videoElement);
  
    const ui = new shaka.ui.Overlay(
      this.player,
      this.videoContainerElement,
      this.videoElement
    );

    const video = this.videoElement;

    video.poster=this.banner;
    const uiElement = video['ui'];
    const config = {
      addSeekBar: true,
      addBigPlayButton: true,
      'controlPanelElements': ['play_pause','time_and_duration','spacer',
      'mute','volume','fullscreen','overflow_menu'
      ],
      'overflowMenuButtons' : ['quality']
    };
    uiElement.configure(config);

    // const controls = ui.getControls();
    // const player = controls.getPlayer();    

    const eventManager = new shaka.util.EventManager();
    eventManager.listen(video, `play`, (event) => {
      this.videoStatus = "playing";
      console.log('playing', event);
      console.log('videoStatus:', this.videoStatus);
    });

    eventManager.listen(video, `pause`, (event) => {
      this.videoStatus = "pause";
      console.log('pause', event);
      console.log('videoStatus:', this.videoStatus);
    });

    eventManager.listen(video, `ended`, (event) => {
      this.videoStatus = "ended";
      console.log('ended', event);
      console.log('videoStatus:', this.videoStatus);
    });

    eventManager.listen(document, `fullscreenchange`, (event) => {
      if (window.screenTop) {
        this.fullscreenflag = false;
        console.log('Full Screen:', this.fullscreenflag);
      } else {
        this.fullscreenflag = true;
        console.log('Full Screen:', this.fullscreenflag);
      }
    });
  
    this.player
      .load(this.manifesturi)
      .then(() => {
        this.videoElement?.play();
      })
      .catch((e: any) => {
        if(e.code === 1001) {
          this.errorInPlaying = true;
          this.errorMsg = 'URL not found';
        }
        console.error("error in playing",e);
      });
  }
}
