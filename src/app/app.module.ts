import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FeedComponent } from './feed/feed.component';
import { ShakaPlayerModule } from './shaka-player/shaka-player.module';



@NgModule({
  declarations: [
    AppComponent,
    FeedComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ShakaPlayerModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
