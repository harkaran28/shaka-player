import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.css']
})
export class FeedComponent implements OnInit {
  url1 = "//storage.googleapis.com/shaka-demo-assets/angel-one/dash.mpd";
  url2 = "https://multiplatform-f.akamaihd.net/i/multi/will/bunny/big_buck_bunny_,640x360_400,640x360_700,640x360_1000,950x540_1500,.f4v.csmil/master.m3u8";
  url3 = "assets/sample-mp4-file.mp4"

  banner1 = "assets/docquity.webp";
  banner2 = "assets/docquity_banner.jpg";
  banner3 = "assets/docquity.png"

  constructor() { }

  ngOnInit(): void {
  }

}
